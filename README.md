# CloudBerry Backup for Linux

This Puppet module can be used to install CloudBerry Backup for Linux,
activate it with Managed Backup Services (MBS) and get a baseline configuration in place.

As of 6/26/2018, only 64-bit versions of the following OS' are supported:
Ubuntu 12/14/16/17, Debian 7/8/9, Suse 11/12, Red Hat 6.x/7.x, Fedora 12/21/26, CentOS 6/7, Oracle Linux 6.x/7.x

## Installation Parameters

When instantiating the class:

* email address and MBS password always required, unless 'ensure => absent' is specified. 
* if 'ensure => absent' is specified, most components will be removed from the host 
* The module accepts `proxy_host` and `proxy_port` parameters, which, being pulled from hiera
allow for automatic configuration of the client to use a proxy server when the host does not have
internet access or specifically requires the use of proxy.
* Password can be specified as a plain text or given as a name of a "service" wallet object.
Of course, it is not recommended to store plain text passwords in Puppet.

Standard installation, with license key and registered email address

```puppet  
  class { 'cloudberry':
	email_address => 'stanford.tcg+dev@stanford.edu',
	proxy_host    => 'cloudberry-proxy.stanford.edu',
	proxy_port    => '3128',
	password      => 'service/its-crc-sg/org/backup'
  }
```

Uninstallation; will remove most things, but not all (see code for details)

```  
  class { 'cloudberry_backup':
    ensure => absent
  }
```

# Backup Plans

The module defines plan resource, which supports all parameters for creating a backup plan. 
Plans hash can be given as `plans` argument to the cloudberry class and defined in hiera
like this:

```yaml
# cloudberry installation
cloudberry::ensure: present
cloudberry::password: 'service/its-crc-sg/org/backup'

# cloudberry backup plans
cloudberry::plans:
  default:
	ensure: present
    plan_name: 'default'
    account: 'unknown'
    include:
      - '/'
    exclude:
      - '/afs'
      - '/dev'
      - '/lib/systemd/system'
      - '/tmp'
      - '/usr/bin'
      - '/urs/lib/jvm'
      - '/usr/share/doc'
      - '/usr/share/man'
      - '/var/lib/lxcfs'
      - '/var/lib/lxd'
      - '/var/ossec/queue'
      - '/var/spool/postfix'
      - '/var/cache'
      - '/var/tmp'
    block_level_backup: false
    backup_empty_dirs: true
    compression: true
    purge: '3m'
    purge_by: 'modifiedDate'
    keep: 7
    keep_last_version: false
    enable_schedule: true
    every: 'day'
    at: '01:13'
    run_missed_on_boot: false
```

Any number of backup plans is supported. Backup plans can be created using this module, but not modified.
After the plan is created, the module does not track down the changes made to it. If a plan is deleted
from cloudberry, the module would recreate it. The module does not remove the backup plans created
manually.

Supported parameters are:

### Backup settings

| Parameter                     | Function
|-------------------------------|-----------------------------------------------------------------------------------------
| $plan_name                    | Plan name
| $account                      | AccountID or AccountName
| $include                      | Backup file or directory
| $exclude                      | Exclude file or directory from Backup
| $include_mask                 | Include files mask. Example: -ifm "*.doc, *.xls"
| $exclude_mask                 | Exclude files mask. Example: -efm "*.o, *.xls"
| $exclude_system_hidden        | Exclude system and hidden files. Possible values: yes(default), no
| $block_level_backup           | Use block level backup. Possible values: yes, no(default)
| $backup_empty_dirs            | Backup empty folders. Possible values: yes, no(default)
| $compression                  | Compress files. Possible values: yes, no(default)
| $notification                 | Specify to recieve notification email: when backup fails 
|                               | "errorOnly"(default) or in all cases "on" or not recieve at all "off"
| $algorithm                    | Encryption algorithm. Possible values: AES_128, AES_192, AES_256, no
| $password                     | Encryption password
| $save_plan_cloud              | Save backup plan configuration to the backup storage. Possible values: yes(default), no
| $s3_transfer_acceleration     | Use Amazon S3 Transfer Acceleration. Possible values: yes, no(default)
| $standard_ia                  | Use Standard-IA. Possible values: yes, no(default)
| $onezone_ia                   | Use Onezone-IA. Possible values: yes, no(default)

### Retention policy

| Parameter                     | Function
|-------------------------------|-----------------------------------------------------------------------------------------
| $purge                        | Purge versions that are older than period (except lastest version). 
|                               | Possible values: no, 1d(day), 1w(week), 1m(month)
| $purge_by                     | Specify date type for purging. Possible values: modifiedDate(default), backupDate
| $keep_last_version            | Always keep the last version. Possible values: yes, no
| $keep                         | Keep limited number of versions. Possible values: all(default), number
| $delete_locally_deleted       | Delete locally deleted files from storage. Possible values: yes, no
| $delete_locally_deleted_delay | Delete locally deleted files delay. Possible values: number of days

### Incremental backup schedule

| Parameter                     | Function
|-------------------------------|-----------------------------------------------------------------------------------------
| $enable_schedule              | Enable schedule. Possible values: yes, no(default)
| $every                        | Specify recurring type. Possible values: day, week, month, dayofmonth
| $at                           | Specify datetime or time of schedule, format "dd.mm.yyyy hh::mm" or "hh:mm". 
|                               | Example: "16.02.2015 12:32" or "12:32" for each recurring schedule
| $work_time                    | Time when plan working, format xx:xx-xx:xx Example 1: 12:32-17:00, 
|                               | Example 2: 23:00-04:00; may be used for each recurring schedule, 
|                               | used only with -recurrencePeriod and without -at
| $recurrance_period            | Specify recurrence period in minutes
| $day                          | Specify day for 'dayofmonth' schedule (1..31)
| $weekday                      | Specify day(s) of week for weekly schedule. 
|                               | Example: "su, mo, tu, we, th, fr, sa". Or specify day of week for monthly schedule
| $weeknumber                   | Specify number of week. Possible values: First, Second, Third, Fourth, Penultimate, Last
| $repeat_every                 | Specify repeat period for Week/Month/DayOfMonth recurrence schedule type
| $repeat_from                  | Specify start date for -repeatEvery for Week/Month/DayOfMonth recurrence schedule type
| $force_stop                   | Stop plan if it executes for more than "hh:mm" (hours : minutes). Possible values: no(default), "hh:mm"
| $run_missed_on_boot           | Run missed scheduled plan immediately upon boot-up. Possible values: yes, no(default)

### Full backup schedule (only for block-level backup)

| Parameter                     | Function
|-------------------------------|-----------------------------------------------------------------------------------------
| $enable_full_schedule         | Enable schedule full backup. Possible values: yes, no(default)
| $every_full                   | Specify recurring type of schedule full backup. 
|                               | Possible values: day, week, month, dayofmonth
| $work_time_full               | Time when plan working, format xx:xx-xx:xx Example 1: 12:32-17:00, 
|                               | Example 2: 23:00-04:00; may be used for each recurring schedule full backup, 
|                               | used only with -recurrencePeriodFull and without -atFull
| $recurrence_period_full       | Specify recurrence period in minutes of schedule full backup
| $at_full                      | Specify datetime or time of schedule full backup, format "hh::mm". 
|                               | Example: "12:32" for each recurring schedule
| $day_full                     | Specify day for 'dayofmonth' schedule full backup (1..31)
| $weekday_full                 | Specify day(s) of week for weekly schedule full backup. 
|                               | Example: "su, mo, tu, we, th, fr, sa". Or specify day of week for monthly schedule
| $weeknumber_full              | Specify number of week for schedule full backup. 
|                               | Possible values: First, Second, Third, Fourth, Penultimate, Last
| $repeat_every_full            | Specify repeat period for Week/Month/DayOfMonth recurrence schedule full backup type
| $repeat_from_full             | Specify start date for -repeatEvery for Week/Month/DayOfMonth recurrence schedule full backup type
| $force_stop_full              | Stop plan if it runs for "hh:mm" minutes. Possible values: no(default), "hh:mm" minutes


