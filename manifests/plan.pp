# definition of a cloudberry backup plan

# ### BACKUP SETTINGS
# #####################################################
# 
# $plan_name                      -n                      Plan name
# $account                        -a                      AccountID or AccountName
# $include                        -f                      Backup file or directory
# $exclude                        -ef                     Exclude file or directory from Backup
# $include_mask                   -ifm                    Include files mask. Example: -ifm "*.doc, *.xls"
# $exclude_mask                   -efm                    Exclude files mask. Example: -efm "*.o, *.xls"
# $exclude_system_hidden          -es                     Exclude system and hidden files. Possible values: yes(default), no
# $block_level_backup             -useBlockLevelBackup    Use block level backup. Possible values: yes, no(default)
# $backup_empty_dirs              -bef                    Backup empty folders. Possible values: yes, no(default)
# $compression                    -c                      Compress files. Possible values: yes, no(default)
# $notification                   -notification           Specify to recieve notification email: when backup fails 
#                                                         "errorOnly"(default) or in all cases "on" or not recieve at all "off"
# $algorithm                      -ea                     Encryption algorithm. Possible values: AES_128, AES_192, AES_256, no
# $password                       -ep                     Encryption password
# $save_plan_cloud                -sp                     Save backup plan configuration to the backup storage. Possible values: yes(default), no
# $s3_transfer_acceleration       -sta                    Use Amazon S3 Transfer Acceleration. Possible values: yes, no(default)
# $standard_ia                    -standardIA             Use Standard-IA. Possible values: yes, no(default)
# $onezone_ia                     -onezoneIA              Use Onezone-IA. Possible values: yes, no(default)
# 
# ### RETENTION POLICY
# #####################################################
# 
# $purge                          -purge                  Purge versions that are older than period (except lastest version). 
#                                                         Possible values: no, 1d(day), 1w(week), 1m(month)
# $purge_by                       -purgeBy                Specify date type for purging. Possible values: modifiedDate(default), backupDate
# $keep_last_version              -keepLastVersion        Always keep the last version. Possible values: yes, no
# $keep                           -keep                   Keep limited number of versions. Possible values: all(default), number
# $delete_locally_deleted         -dl                     Delete locally deleted files from storage. Possible values: yes, no
# $delete_locally_deleted_delay   -dld                    Delete locally deleted files delay. Possible values: number of days
# 
# ### INCREMENTAL BACKUP SCHEDULE. 
# #####################################################
# 
# $enable_schedule                -en                     Enable schedule. Possible values: yes, no(default)
# $every                          -every                  Specify recurring type. Possible values: day, week, month, dayofmonth
# $at                             -at                     Specify datetime or time of schedule, format "dd.mm.yyyy hh::mm" or "hh:mm". 
#                                                         Example: "16.02.2015 12:32" or "12:32" for each recurring schedule
# $work_time                      -workTime               Time when plan working, format xx:xx-xx:xx Example 1: 12:32-17:00, 
#                                                         Example 2: 23:00-04:00; may be used for each recurring schedule, 
#                                                         used only with -recurrencePeriod and without -at
# $recurrance_period              -recurrencePeriod       Specify recurrence period in minutes
# $day                            -day                    Specify day for 'dayofmonth' schedule (1..31)
# $weekday                        -weekday                Specify day(s) of week for weekly schedule. 
#                                                         Example: "su, mo, tu, we, th, fr, sa". Or specify day of week for monthly schedule
# $weeknumber                     -weeknumber             Specify number of week. Possible values: First, Second, Third, Fourth, Penultimate, Last
# $repeat_every                   -repeatEvery            Specify repeat period for Week/Month/DayOfMonth recurrence schedule type
# $repeat_from                    -repeatFrom             Specify start date for -repeatEvery for Week/Month/DayOfMonth recurrence schedule type
# $force_stop                     -forceStop              Stop plan if it executes for more than "hh:mm" (hours : minutes). Possible values: no(default), "hh:mm"
# $run_missed_on_boot             -runMissed              Run missed scheduled plan immediately upon boot-up. Possible values: yes, no(default)
# 
# ### FULL BACKUP SCHEDULE. Only for block-level backup
# #####################################################
# 
# $enable_full_schedule           -enFull                 Enable schedule full backup. Possible values: yes, no(default)
# $every_full                     -everyFull              Specify recurring type of schedule full backup. 
#                                                         Possible values: day, week, month, dayofmonth
# $work_time_full                 -workTimeFull           Time when plan working, format xx:xx-xx:xx Example 1: 12:32-17:00, 
#                                                         Example 2: 23:00-04:00; may be used for each recurring schedule full backup, 
#                                                         used only with -recurrencePeriodFull and without -atFull
# $recurrence_period_full         -recurrencePeriodFull   Specify recurrence period in minutes of schedule full backup
# $at_full                        -atFull                 Specify datetime or time of schedule full backup, format "hh::mm". 
#                                                         Example: "12:32" for each recurring schedule
# $day_full                       -dayFull                Specify day for 'dayofmonth' schedule full backup (1..31)
# $weekday_full                   -weekdayFull            Specify day(s) of week for weekly schedule full backup. 
#                                                         Example: "su, mo, tu, we, th, fr, sa". Or specify day of week for monthly schedule
# $weeknumber_full                -weeknumberFull         Specify number of week for schedule full backup. 
#                                                         Possible values: First, Second, Third, Fourth, Penultimate, Last
# $repeat_every_full              -repeatEveryFull        Specify repeat period for Week/Month/DayOfMonth recurrence schedule full backup type
# $repeat_from_full               -repeatFromFull         Specify start date for -repeatEvery for Week/Month/DayOfMonth recurrence schedule full backup type
# $force_stop_full                -forceStopFull          Stop plan if it runs for "hh:mm" minutes. Possible values: no(default), "hh:mm" minutes


define cloudberry::plan (
  String                                      $plan_name = name,
  Enum['absent', 'present']                   $ensure,
  String                                      $account = undef,
  Array[String]                               $include_file = [],
  Array[String]                               $exclude_file = [],
  Array[String]                               $include_mask = [],
  Array[String]                               $exclude_mask = [],
  Boolean                                     $exclude_system_hidden = false,
  Boolean                                     $block_level_backup = false,
  Boolean                                     $backup_empty_dirs = true,
  Boolean                                     $compression = false,
  Boolean                                     $save_plan_cloud = false,
  Enum['errorOnly', 'on', 'off']              $notification = 'errorOnly',
  Variant[Boolean, Enum['AES_128', 'AES_192', 'AES_256']] $algorithm = false,
  String                                      $password = "service/its-crc-sg/${::fqdn_lc}/backup",
  Boolean                                     $s3_transfer_acceleration = false,
  Boolean                                     $standard_ia = false,
  Boolean                                     $onezone_ia = false,

  Variant[Boolean, String]                    $purge = false,
  Enum['modifiedDate', 'backupDate']          $purge_by = 'modifiedDate',
  Boolean                                     $keep_last_version = false,
  Variant[Enum['all'], Integer]               $keep = 'all',
  Boolean                                     $delete_locally_deleted = false,
  Integer                                     $delete_locally_deleted_delay = 0,

  Boolean                                     $enable_schedule = false,
  Enum['day', 'week', 'month', 'dayofmonth']  $every = 'day',
  Optional[String]                            $at = '00:00',
  Optional[String]                            $work_time = undef,
  Optional[Integer[0,60]]                     $recurrance_period = undef,
  Optional[Integer[1,31]]                     $day = undef,
  Array[Enum['su', 'mo', 'tu', 'we', 'th', 'fr', 'sa']] $weekday = ['mo'],
  Enum['First', 'Second', 'Third', 'Fourth', 'Penultimate', 'Last'] $weeknumber = 'First',
  Integer                                     $repeat_every = 1,
  String                                      $repeat_from = "${strftime('%d.%m.%Y')}",
  Variant[Boolean, String]                    $force_stop = false,
  Boolean                                     $run_missed_on_boot = false,

  Boolean                                     $enable_full_schedule = false,
  Enum['day', 'week', 'month', 'dayofmonth']  $every_full = 'day',
  Optional[String]                            $work_time_full = undef,
  Integer[0,60]                               $recurrence_period_full = 0,
  Optional[String]                            $at_full = '00:00',
  Integer[1,31]                               $day_full = 1,
  Array[Enum['su', 'mo', 'tu', 'we', 'th', 'fr', 'sa']] $weekday_full = ['mo'],
  Enum['First', 'Second', 'Third', 'Fourth', 'Penultimate', 'Last'] $weeknumber_full = 'First',
  Integer                                     $repeat_every_full = 1,
  String                                      $repeat_from_full = "${strftime('%d.%m.%Y')}",
  Variant[Boolean, String]                    $force_stop_full = false,
) {

  if ($ensure == 'present') {

    # process inclusions and exclusions
    $include_str = empty($include_file) ? {
      false   => "-f ${join($include_file, ' -f ')}",
      default => '',
    }

    $exclude_str = empty($exclude_file) ? {
      false   => "-ef ${join($exclude_file, ' -ef ')}",
      default => '',
    }

    $include_mask_str = empty($include_mask) ? {
      false   => "-ifm ${join($include_mask, ' -ifm ')}",
      default => '',
    }

    $exclude_mask_str = empty($exclude_mask) ? {
      false   => "-efm ${join($exclude_mask, ' -efm ')}",
      default => '',
    }

    if ($standard_ia or $onezone_ia) {
      $storage_class = "-standardIA ${bool2yesno($standard_ia)} -onezoneIA ${bool2yesno($onezone_ia)}"
    } else {
      $storage_class = ''
    }

    # process encryption. if algorithm is boolean, i.e. "false",
    # do not include encryption parameters at all
    if ($algorithm =~ Boolean) {
      $encryption_params = ''
    } else {
      # if password starts with "service/", then consider it a wallet object
      # otherwise use it as a plain text password
      if ($password =~ /^service\//) {
        $password_str = "$(/usr/bin/k5start -Uq -f /etc/krb5.keytab -- /usr/bin/wallet get file ${password})"
      } else {
        $password_str = "'$password'"
      }
      $encryption_params = "-ea ${algorithm} -ep ${password_str}"
    }

    # purge settings. if purge is boolean i.e. "false",
    # do not include the parameter altogether
    if ($purge =~ Boolean) {
      $purge_params = ''
    } else {
      $purge_params = "-purge ${purge} -purgeBy ${purge_by}"
    }

    # delete locally deleted
    if ($delete_locally_deleted) {
      $delete_local_params = "-dl ${bool2yesno($delete_locally_deleted)} -dld ${delete_locally_deleted_delay}"
    } else {
      $delete_local_params = ''
    }

    # schedules
    if ($enable_schedule) {
      # backup frequencies
      # daily frequency is always available
      if ($work_time != undef) and ($recurrence != undef) {
        $when = "-workTime ${work_time} -recurrencePeriod ${recurrence_period}"
      } elsif ($at != undef) {
        $when = "-at ${at}"
      } else {
        fail('Please specify when backups are supposed to occur.')
      }

      # other frequencies
      case $every {
        'week': {
          $schedule = "-weekday '${join($weekday, ', ')}'"
        }
        'month': {
          $schedule = "-weeknumber ${weeknumber} -weekday '${join($weekday, ', ')}' -repeatEvery ${repeat_every} -repeatFrom ${repeat_from}"
        }
        'dayofmonth': {
          $schedule = "-day ${day}"
        }
        default: {
          $schedule = ''
        }
      }

      # if force_stop is a boolean false,
      # convert it to a string "no"
      if ($force_stop =~ Boolean) {
        $force_stop_str = 'no'
      } else {
        $force_stop_str = $force_stop
      }

      $schedule_params = "-en ${bool2yesno($enable_schedule)} -every ${every} ${when} ${schedule} -forceStop ${force_stop_str} -runMissed ${bool2yesno($run_missed_on_boot)}"
    } else {
      $schedule_params = ''
    }

    # use block level backup if requested
    $block_level_params = "-useBlockLevelBackup ${bool2yesno($block_level_backup)}"

    # schedules full backups when block level backups are enabled
    if ($block_level_backup) and ($enable_full_schedule){
      # backup frequencies
      # daily frequency is always available
      if ($work_time_full != undef) and ($recurrence_full != undef) {
        $when_full = "-workTimeFull ${work_time} -recurrencePeriodFull ${recurrence_period_full}"
      } elsif ($at != undef) {
        $when_full = "-atFull ${at_full}"
      } else {
        fail('Please specify when block level backups are supposed to occur.')
      }

      # other frequencies
      case $every_full {
        'week': {
          $schedule_full = "-weekdayFull '${join($weekday_full, ',')}'"
        }
        'month': {
          $schedule_full = "-weeknumberFull ${weeknumber_full} -weekdayFull '${join($weekday_full, ', ')}' -repeatEveryFull ${repeat_every_full} -repeatFromFull ${repeat_from_full}"
        }
        'dayofmonth': {
          $schedule_full = "-dayFull ${day_full}"
        }
        default: {
          $schedule_full = ''
        }
      }

      # if force_stop_full is a boolean false,
      # convert it to a string "no"
      if ($force_stop_full =~ Boolean) {
        $force_stop_full_str = 'no'
      } else {
        $force_stop_full_str = $force_stop_full
      }

      $schedule_params_full = "-enFull ${bool2yesno($enable_full_schedule)} -everyFull ${every_full} ${when_full} ${schedule_full} -forceStopFull ${force_stop_full_str}"
    } else {
      $schedule_params_full = ''
    }

    $params = "addBackupPlan -n ${plan_name} -a ${account} ${storage_class} ${include_str} ${exclude_str} ${include_mask_str} ${exclude_mask_str} -es ${bool2yesno($exclude_system_hidden)} -sp ${bool2yesno($save_plan_cloud)} -c ${bool2yesno($compression)} ${encryption_params} -bef ${bool2yesno($backup_empty_dirs)} -keepLastVersion ${bool2yesno($keep_last_version)} -keep ${keep} ${purge_params} ${delete_local_params} ${schedule_params} ${block_level_params} ${schedule_params_full} -notification ${notification}"

    ### debugging ###
    # notify { "$params": }

    exec { "create-${plan_name}-plan":
      path    => '/bin:/usr/bin:/usr/sbin:/opt/local/Stanford TCG Cloud Backup/bin',
      unless  => "cbb plan -l | grep -q ${plan_name}",
      onlyif  => "cbb account -l | grep ${account}",
      command => "cbb ${params}",
    }

  } else {
    # resource is absent. delete the plan if it exists
    exec { "delete-${plan_name}-plan":
      path    => '/bin:/usr/bin:/usr/sbin:/opt/local/Stanford TCG Cloud Backup/bin',
      onlyif  => "cbb plan -l | grep -q ${plan_name}",
      command => "cbb deletePlan -n ${plan_name}",
    }
  }
}
