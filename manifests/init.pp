# Install and configure CloudBerry Backup for Linux
# As of 6/26/2018, supported on 64-bit EL6+, Debian 7+, Ubuntu 12.04+
#
# Packages are delivered from the stanford-tcg repo on yum.stanford.edu and debian.stanford.edu.
# under the original package name stanford-tcg-cloud-backup. 
#
# Parameters
#   ensure:          present - install agent (default), absent -- remove agent.
#   email_address:   email address of the MSB account. Typically stanford-tcg+client_name@gmail.com
#   proxy_host:      proxy sever to use. Typically set from hiera to cloudberry-proxy.stanford.edu.
#   proxy_port:      port on the proxy_host
#
# Password for the MSB account is stored in wallet as "app/server.stanford.edu/backup".
#

class cloudberry (
  $ensure        = 'present',
  $email_address = undef,
  $proxy_host    = undef,
  $proxy_port    = undef,
  $password      = "service/its-crc-sg/${::fqdn_lc}/backup",
  $threads       = '5',
  $plans         = {},
) {

  # Only x86_64 installers are available as of August 2017
  if (($::architecture != 'x86_64') and ($::architecture != 'amd64')) {
    fail ('CloudBerry Backup for linux is only available on 64-bit architectures')
  }

  # Only on EL6+, Ubuntu 12+ (NO Debian)
  if ((($::osfamily == 'RedHat') and ($::lsbmajdistrelease < '6')) or
      (($::operatingsystem == 'Ubuntu') and ($::lsbmajdistrelease < '12.04'))) {
    fail ('CloudBerry Backup for linux is only available on EL6+, Debian, and Ubuntu 12.04+')
  }

  # Both license_key and email_address are required (though license_key can be 'trial')
  if (($email_address == undef) and ($ensure == 'present')) {
    fail('For CloudBerry backup, email-address is a required parameters')
  }

  # path to the binary
  $cbb = '"/opt/local/Stanford TCG Cloud Backup/bin/cbb"'

  case $ensure {
    'present': {
      $service_ensure = 'running'
      $service_enable = true
      $ensure_link = 'link'
    }
    'absent': {
      $service_ensure = 'stopped'
      $service_enable = false
      $ensure_link = 'absent'
    }
  }

  # install cloudberry from tcg repos
  package { 'stanford-tcg-cloud-backup':
    ensure => $ensure,
  }

  # create convenient links
  file { '/usr/local/bin/cbb':
    ensure  => $ensure_link,
    target  => '/opt/local/Stanford TCG Cloud Backup/bin/cbb',
    require => Package['stanford-tcg-cloud-backup'],
  }

  file { '/usr/local/bin/cbbgui':
    ensure  => $ensure_link,
    target  => '/opt/local/Stanford TCG Cloud Backup/bin/cbbgui',
    require => Package['stanford-tcg-cloud-backup'],
  }

  # set proxy before attempting to activate, when installing the package
  # remove proxy if there is no need for it any longer
  exec { 'set-proxy':
    command     => $proxy_host ? {
      undef   => "${cbb} option -set -proxy -pt no",
      default => "${cbb} option -set -proxy -pt manual -pa '${proxy_host}' -pp ${proxy_port}",
    },
    refreshonly => true,
    require     => Package['stanford-tcg-cloud-backup'],
  }

  if ($password =~ /^service\//) {
    $password_str = "$(/usr/bin/k5start -Uq -f /etc/krb5.keytab -- /usr/bin/wallet get file ${password})"
  } else {
    $password_str = "'$password'"
  }

  # activate cloudberry with user name and password
  # if not already activated.
  exec { 'activate-production':
    command => "${cbb} addAccount -e '${email_address}' -p ${password_str} -edition Desktop",
    onlyif  => "${cbb} account -l | grep 'You cannot use'",
    require => [ Package['stanford-tcg-cloud-backup'], Exec['set-proxy'] ],
  }

  # set thread count
  ~> exec { 'set-thread-count':
    command     => "${cbb} option -set -threadcount -v ${threads}",
    refreshonly => true,
  }

  # create backup plans
  create_resources('cloudberry::plan', $plans)

  # Run the services
  # Backup service
  service { 'stanford-tcg-cloud-backup':
    ensure    => $service_ensure,
    enable    => $service_enable,
    require   => Package['stanford-tcg-cloud-backup'],
    hasstatus => true,
  }

  # Remote management
  service { 'stanford-tcg-cloud-backupRM':
    ensure    => $service_ensure,
    enable    => $service_enable,
    require   => Package['stanford-tcg-cloud-backup'],
    hasstatus => true,
  }

  # Web access
  service { 'stanford-tcg-cloud-backupWA':
    ensure    => $service_ensure,
    enable    => $service_enable,
    require   => Package['stanford-tcg-cloud-backup'],
    hasstatus => true,
  }

}
